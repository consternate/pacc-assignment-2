void printMatrix(int size, double** a);
void matmul_ijk(int size, double** a, double** b, double** result);
void matmul_ijk_blocked(int size, int blocksize, double** a, double** b, double** result);
void matmul_kij_blocked(int size, int blocksize, double** a, double** b, double** result);
void fillMatrix(int size, double** mtrix);
