#include <stdio.h>
#include <cblas.h>

void matmul_kij_blocked(int n, int blockSize, double** a, double** b, double** result) {

   if (n % blockSize != 0) {
      printf("Error. Size must be multiple of blocksize.");
      return;
   }

   int i, j, k, bi, bj, bk;

   for(bk = 0; bk < n; bk += blockSize) {
      for(bi = 0; bi < n; bi += blockSize) {
         for(bj = 0; bj < n; bj += blockSize) {
            // result[bi + i][bj + j] += a[bi + i][bk + k] * b[bk + k][bj + j];
            // we have to always add results to the bi^th row-block of result 
            // and the bj^th column while iterating with bk blockwise
            // through a and b. By pointer arithmetic, we can do that.
            cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                  blockSize, blockSize, blockSize, 1.0, a[bi] + bk, n,
                  b[bk] + bj, n, 1.0, result[bi] + bj, n);
         }
      }
   }
}
