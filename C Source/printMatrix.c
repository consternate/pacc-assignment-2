#include <stdio.h>

void printMatrix(int size, double** a) {

   int i;
   int j;
   for (i = 0; i < size; i++) {
      for (j = 0; j < size; j++) {
         printf("%20lf", a[i][j]);
      }
      printf("\n");
   }
}


