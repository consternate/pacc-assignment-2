#include <stdio.h>
#include <math.h>

void matmul_ijk_blocked(int n, int blockSize, double** a, double** b, double** result) {

   if (n % blockSize != 0) {
      printf("Error. Size must be multiple of blocksize.");
      return;
   }

   int i, j, k, bi, bj, bk;

   for(bi = 0; bi < n; bi += blockSize) {
      for(bj = 0; bj < n; bj += blockSize) {
         for(bk = 0; bk < n; bk += blockSize) {
            for(i = 0; i < blockSize; i++) {
               for(j = 0; j < blockSize; j++) {
                  for(k = 0; k < blockSize; k++) {
                     result[bi + i][bj + j] += a[bi + i][bk + k] * b[bk + k][bj + j];
                  }
               }
            }
         }
      }
   }
}
