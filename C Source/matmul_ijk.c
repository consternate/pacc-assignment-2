void matmul_ijk(int size, double** a, double** b, double** result) {

   int i, j, k;

   for (i = 0; i < size; i++) {
      for (j = 0; j < size; j++) {
         for (k = 0; k < size; k++) {
            result[i][j] += a[i][k] * b[k][j];
         }
      }
   }
}

