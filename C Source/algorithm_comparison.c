#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <cblas.h>
#include "assignment2.h"

static double runMultiply(int n, int blockSize, int choice) {

   double **a, **b, **result;
   int i, j, k;

   /* Init matrices*/
   a = malloc(n * sizeof(double*));
   b = malloc(n * sizeof(double*));
   result = malloc(n * sizeof(double*));

   if (a == NULL || b == NULL || result == NULL) {
      printf("Error.\n");
      return -1;
   }
   a[0] = (double *)malloc(n*n*sizeof(double));
   b[0] = (double *)malloc(n*n*sizeof(double));
   result[0] = (double *)calloc(n*n,sizeof(double));

   // THE WAY THIS IS DONE IS IMPORTANT SO THAT ALL MEMORY IS CONSECUTIVE
   for (i = 0; i < n; i++) {
      a[i] = a[0]+i*n;
      b[i] = b[0]+i*n;
      result[i] = result[0]+i*n;
   }

   fillMatrix(n, a);
   fillMatrix(n, b);

   /* printf("Matrix a:\n"); */
   /* printMatrix(n, a); */
   /* printf("Matrix b:\n"); */
   /* printMatrix(n, b); */

   /*-------------*/

   // timing
   struct timeval tv1, tv2;
   struct timezone tz;

   switch(choice) {
      case 1:
         gettimeofday(&tv1, &tz);
         matmul_ijk(n, a, b, result);
         gettimeofday(&tv2, &tz);
         break;
      case 2:
         gettimeofday(&tv1, &tz);
         matmul_ijk_blocked(n, blockSize, a, b, result);
         gettimeofday(&tv2, &tz);
         break;
      case 3:
         gettimeofday(&tv1, &tz);
         matmul_kij_blocked(n, blockSize, a, b, result);
         gettimeofday(&tv2, &tz);
         break;
      case 4:
         gettimeofday(&tv1, &tz);
         cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, n, n, n, 1.0,
               a[0], n, b[0], n, 0.0, result[0], n);
         gettimeofday(&tv2, &tz);
         break;
      default:
         puts("Error. Mode not recognized.");
         return 1;
   }

   // Result
   /* printf("Result a*b:\n"); */
   /* printMatrix(n, result); */

   // Print time
   double elapsed = (double) (tv2.tv_sec-tv1.tv_sec) 
      + (double) (tv2.tv_usec-tv1.tv_usec) * 1.e-6;
   // printf("Time elapsed: %lf\n", elapsed);

   free(a[0]);
   free(b[0]);
   free(result[0]);
   free(a);
   free(b);
   free(result);

   return elapsed;
}

int main(int argc, const char *argv[]) {

   int i, j, blockSize, choice;

   /* USER INTERACTION MODE */
   int n;
   printf("Enter n: ");
   scanf("%d", &n);
   printf("Enter block size: ");
   scanf("%d", &blockSize);

   do {
      printf("Enter method (1 -> naive, 2 -> ijk-blocked, " 
            "3 -> kij-blocked, 4 -> cblas_dgemm): ");
      scanf("%d", &choice);
   } while (choice < 1 || choice > 4);

   double status = runMultiply(n, blockSize, choice);
   if (status < 0) {
      printf("Error.\n");
      return 1;
   }

   printf("Time elapsed: %lf seconds\n", status);
   return 0;

   /* /1* AUTMATIC MODE *1/ */
   /* FILE *naive = fopen("outputs/naive6.txt", "w"), */ 
   /*      *blocked_ijk = fopen("outputs/blocked_ijk6.txt", "w"), */
   /*      *blocked_kij = fopen("outputs/blocked_kij6.txt", "w"), */
   /*      *atlas = fopen("outputs/atlas6.txt", "w"); */
   /*      // *blocksize_vec = fopen("outputs/blocksizes.txt", "w"), */
   /*      // *size_vec = fopen("outputs/matsizes.txt", "w"); */

   /* int sizes[] = { 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048}; */
   /* register int n; */
   /* int blocks_ind; */
   /* double t_naive, t_ijk, t_kij, t_atlas; */
   /* for (i = 0; i <= 10; i++) { */
   /*    n = sizes[i]; */
   /*    // fprintf(size_vec, "%d ", n); */
   /*    printf("Done for "); */
   /*    t_naive = runMultiply(n, 0, 1); */
   /*    t_atlas = runMultiply(n, 0, 4); */
   /*    for (blocks_ind = 0; blocks_ind <= 10; blocks_ind++) { */
   /*       blockSize = sizes[blocks_ind]; */
   /*       if(blockSize <= n) { */
   /*          t_ijk = runMultiply(n, blockSize, 2); */
   /*          t_kij = runMultiply(n, blockSize, 3); */

   /*          fprintf(naive, "%2.15lf ", t_naive); */
   /*          fprintf(blocked_ijk, "%2.15lf ", t_ijk); */
   /*          fprintf(blocked_kij, "%2.15lf ", t_kij); */
   /*          fprintf(atlas, "%2.15lf ", t_atlas); */
   /*          printf("%d ", blockSize); */
   /*       } else { */
   /*          fprintf(naive, "%8s ", "nan"); */
   /*          fprintf(blocked_ijk, "%8s ", "nan"); */
   /*          fprintf(blocked_kij, "%8s ", "nan"); */
   /*          fprintf(atlas, "%8s ", "nan"); */
   /*       } */
   /*    } */
   /*    fprintf(naive, "\n"); */
   /*    fprintf(blocked_ijk, "\n"); */
   /*    fprintf(blocked_kij, "\n"); */
   /*    fprintf(atlas, "\n"); */
   /*    printf("\n\tdone for n = %d.\n", n); */
   /* } */
   /* fclose(naive); */
   /* fclose(blocked_ijk); */
   /* fclose(blocked_kij); */
   /* fclose(atlas); */
   /* // fclose(blocksize_vec); */
   /* // fclose(size_vec); */
}
