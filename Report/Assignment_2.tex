\documentclass{article}
\title{Parallel \& Cluster Computing -- Assignment 2}
\author{Rasmus Diederichsen}
\date{\today}
\usepackage{microtype,
            xcolor,
            array,
            amsmath,
            amssymb,
            graphicx,
            subcaption,
            lmodern}
\usepackage[pdftitle={Parallel \& Cluster Computing -- Assignment 2}, 
       pdfauthor={Rasmus Diederichsen}, 
       hyperfootnotes=true,
       colorlinks,
       bookmarksnumbered = true,
       linkcolor = blue,
       plainpages = false,
       citecolor = blue]{hyperref}
\usepackage[T1]{fontenc}
\usepackage[all]{hypcap}
\usepackage{txfonts}

\begin{document}
   
   \maketitle

   \thispagestyle{empty}
   \pagestyle{empty}

   All computations have been performed single-threaded on an Intel i5-3210M
   Dual Core Processor with a nominal clock frequency of 2.5 Ghz, though I don't
   know under which circumstances this value is boosted to its maximum of 3.1
   Ghz. The level 1 cache is reportedly 128 kilobytes. 

   Multiplication is performed on matrices whose dimensions are the powers of 2
   ($2^1$ through $2^{11}$). The block sizes take the same values. 6
   measurements have been averaged to obtain more noise-free results. For the
   algorithms which don't employ any blocking, the effect of different block
   sizes has not been incorporated in the program after it was eyeballed and
   unsurprisingly found to be only noise. Plots for these include the variable
   block size nonetheless due to laziness of the programmer. The multiplications
   for $n = 2048$ for all block sizes takes about 15 minutes which is why I do
   not intend to perform it on the csserver. For such a machine, the range of
   $n$ should be limited to $2^{10}$ or lower.

   Plots have been produced in \textsc{MatLab} using fabulous functions by Gabe
   Hoffmann and Ken Garrard, saving me many a hassle.

   In all plots featuring a colorbar, the uppermost label (the top YTick
   element in \textsc{MatLab}) has been omitted to signal that the uppermost
   colour could represent any arbitrarily high value. The problem at hand is
   that for algorithms with this complexity, all runtimes will look miniscule
   when compared to the highest input ($n = 2048$) and with default parameters,
   these values would be coloured in the topmost colour, but the runtimes of all
   other parameter combinations would be so small in comparison, that their
   colouring is collapsed into 2-3 shades of blue. This makes them less easily
   distinguishable. I have therefore resorted to mapping all the lower values to
   the greatest portion of the colorbar (by adjusting the \texttt{CLim}
   property) and leaving out the upper most label to indicate an ``open end''.

   Finally, a
   \href{https://bitbucket.org/consternate/pacc-assignment-2/src}{public
   BitBucket repository} containing all code is made available, should the
   instructor want to verify (or falsify) the correctness of the data, or generate the
   plots themselves. Therein can be found also a Makefile to build the
   executable on the CS server.

   \section{Runtime dependence on matrix an block size} 
   
   What follows are sets of figures displaying for each algorithm a cloud plot
   showing each variable (matrix size, block size, runtime) on its own axis and
   another plot colour-coding the block size.

   \subsection{Naive $ijk$-algorithm} 

   Refer to figures \ref{ijk_cloud} and \ref{ijk_scatter}. The scatter plot
   includes all block sizes but the markers are all behind each other because it
   was not taken into account during the calculations.

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/ijk_runtime_cloud.pdf}
         \caption{Cloud plot for the naive $ijk$-algorithm}
         \label{ijk_cloud}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/ijk_runtime_scatter.pdf}
         \caption{Scatter plot for the naive $ijk$-algorithm}
         \label{ijk_scatter}
      \end{subfigure}
      \caption{Plots for the naive $ijk$-algorithm}
   \end{figure}


   \subsection{Blocked $ijk$-algorithm} 

   Figures for the blocked $ijk$-algorithm are displayed as
   \autoref{blocked_ijk_cloud} and \autoref{blocked_ijk_scatter}. In the latter
   especially we see that we get a most ignificant speedup for block sizes 4 to
   64. Although I don't really see how this fits with the fact that my L1 cache
   is supposed to fit 128 thousand bytes.
   
   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
      \includegraphics[width=\textwidth]{../plots/blocked_ijk_runtime_cloud.pdf}
      \caption{Cloud plot for the blocked $ijk$-algorithm}
      \label{blocked_ijk_cloud}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
      \includegraphics[width=\textwidth]{../plots/blocked_ijk_runtime_scatter.pdf}
      \caption{Scatter plot for the blocked $ijk$-algorithm}
      \label{blocked_ijk_scatter}
      \end{subfigure}
      \caption{Plots for the blocked $ijk$-algorithm}
   \end{figure}

   \subsection{Blocked $kij$-algorithm with \textsc{Atlas}}

   The variant of $kij$ used here uses \textsc{Atlas}'s version of
   \texttt{cblas\_dgemm} to multiply the submatrices. The results are dispalyed
   in figures \ref{kij_cloud} and \ref{kij_scatter}. 

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/kij_runtime_cloud.pdf}
         \caption{Cloud plot for $kij$ with \textsc{Atlas} routine}
         \label{kij_cloud}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/kij_runtime_scatter.pdf}
         \caption{Scatter plot for $kij$ with \textsc{Atlas} routine}
         \label{kij_scatter}
      \end{subfigure}
      \caption{Plots for the blocked $kij$-algorithm with \textsc{Atlas} help}
   \end{figure}

   \subsection{Pure \textsc{Atlas} routine} 
   
   Lastly, pure \textsc{Atlas} routine has been tested which was unsurprisingly
   the fastest. It also does not depend on the block size, I assume at
   compilation, the optimal block size has been determined for my computer.
   Note the different scale with respect to runtime compared to the previous
   examples in \autoref{atlas_cloud} and \autoref{atlas_scatter}.

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/atlas_runtime_cloud.pdf}
         \caption{Cloud plot for \textsc{Atlas} routine}
         \label{atlas_cloud}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/atlas_runtime_scatter.pdf}
         \caption{Scatter plot for \textsc{Atlas} routine}
         \label{atlas_scatter}
      \end{subfigure}
      \caption{Plots for the pure \textsc{Atlas} routine}
   \end{figure}

   \section{Task 2} 
   
   Speed-ups have been calculated for the blocked algorithms over the non-blocked
   naive $ijk$-algorithm and are specified in terms of the quotient of the
   runtime of the naive algorithm and the respective blocked one, i.e. how many
   times as fast the blocked algorithm is with the naive algorithm being the
   baseline. The results are plotted in \autoref{spuijk}, \autoref{spuijk_top},
   \autoref{spukij} and \autoref{spukij_top}. In addition to
   the cloud plot, a top view of the same plot is provided for easier access on
   a two-dimensional medium. 

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_blocked_ijk_ijk.pdf}
         \caption{Speed-up of blocked $ijk$ over naive $ijk$}
         \label{spuijk}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_blocked_ijk_ijk_topview.pdf}
         \caption{Top view of the previous plot}
         \label{spuijk_top}
      \end{subfigure}
   \end{figure}

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_kij_ijk.pdf}
         \caption{Speed-up of blocked $kij$ over naive $ijk$}
         \label{spukij}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_kij_ijk_topview.pdf}
         \caption{Top view of the previous plot}
         \label{spukij_top}
      \end{subfigure}
   \end{figure}

   Unsurprisingly, the blocked algorithms greatly outperform the naive one. This
   one in the worst case produces a cache miss for every $k$ it iterates through
   (since one matrix is accessed by column), such that per computed element at
   least $n$ cache misses occur (given that no more than one row of the second
   matrix fits into cache). The cache misses are thus in the order of $n^3$.
   This is mitigated somewhat by the blocked algorithms which try to reuse data
   that is already in cache. The blocked $ijk$-algorithm decreases the number of
   cache misses by iterating over the columns of the second matrix blockwise,
   that is, rows (or partial rows) are loaded into cache and then instead of
   iterating over all rows, the algorithm iterates over all partial columns in
   these partial rows and thus reuses the data already in cache before replacing
   it with new rows. The blocked $kij$ algorithm also tiles the matrices into
   submatrices and feeds them to \texttt{cblas\_dgemm}, and also puts the
   $k$-loop to the front.

   \section{Comparison with the \textsc{Atlas} reference} 
   
   The blocked $kij$-algorithm is the fastest of the three custom
   implementation, which is largely a result of its employing \textsc{Atlas} to
   do the heavy lifting, I think. Since the \textsc{Atlas} routine minimises
   cache misses in general, it also minimises them on the submatrices which
   $kij$ is giving it. This naturally also reduces the number of misses over the
   whole matrix, but is not the globally optimal way to multiply it. Figures
   \ref{spuakij} and \ref{spuakij_top} show that \textsc{Atlas} is still a lot
   faster though, probably because it employs all kinds of optimisation
   techniques while the other algorithm uses only cache blocking.

   \begin{figure}
      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_atlas_kij.pdf}
         \caption{Speed-up of \textsc{Atlas} routine over blocked $kij$}
         \label{spuakij}
      \end{subfigure}

      \begin{subfigure}[b]{\textwidth}
         \includegraphics[width=\textwidth]{../plots/speedup_atlas_kij_topview.pdf}
         \caption{Top view of the previous plot}
         \label{spuakij_top}
      \end{subfigure}
   \end{figure}
\end{document}
