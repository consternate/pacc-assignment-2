%% Load shit
clear;
clc;
close all;

blocks = 2.^(1:11);
matsizes = load ('../outputs/matsizes.txt');

naive = load('../outputs/naive.txt');
naive2 = load('../outputs/naive2.txt');
naive3 = load('../outputs/naive3.txt');
naive4 = load('../outputs/naive4.txt');
naive5 = load('../outputs/naive5.txt');
naive6 = load('../outputs/naive6.txt');
naive = (naive+naive2+naive3+naive4+naive5+naive6)/6;

ijk = load('../outputs/blocked_ijk.txt');
ijk2 = load('../outputs/blocked_ijk2.txt');
ijk3 = load('../outputs/blocked_ijk3.txt');
ijk4 = load('../outputs/blocked_ijk4.txt');
ijk5 = load('../outputs/blocked_ijk5.txt');
ijk6 = load('../outputs/blocked_ijk6.txt');
ijk = (ijk+ijk2+ijk3+ijk4+ijk5+ijk6)/6;

kij = load('../outputs/blocked_kij.txt');
kij2 = load('../outputs/blocked_kij2.txt');
kij3 = load('../outputs/blocked_kij3.txt');
kij4 = load('../outputs/blocked_kij4.txt');
kij5 = load('../outputs/blocked_kij5.txt');
kij6 = load('../outputs/blocked_kij6.txt');
kij = (kij+kij2+kij3+kij4+kij5+kij6)/6;

atlas = load('../outputs/atlas.txt');
atlas2 = load('../outputs/atlas2.txt');
atlas3 = load('../outputs/atlas3.txt');
atlas4 = load('../outputs/atlas4.txt');
atlas5 = load('../outputs/atlas5.txt');
atlas6 = load('../outputs/atlas6.txt');
atlas = (atlas+atlas2+atlas3+atlas4+atlas5+atlas6)/6;


marker = {'.', 20};
colours = flipud(jet(11)); % for scatter../plots

addpath('/Users/Rasmus/Documents/Uni/Semester 5 - UCD/Parallel and Cluster Computing/Assignments/2/MatLab Source');
%% Plot naive algorithm
fnaive = figure;
plot3k(naive, 'Marker', marker, 'ColorRange', [0, 20]);
cbar = findobj(gcf, 'Tag', 'Colorbar');
ytick = get(cbar, 'YTickLabel');
set(cbar, 'YTickLabel', [ytick(1:end-1); {''}]);
ylabel('log_2 of matrix size');
xlabel('log_2 of blocksize (no effect)');
title(sprintf('Dependence of runtime (in seconds) on matrix and block size\n in O(n^3) naive algorithm'));
saveas(gcf, '../plots/figs/fnaive', 'fig');

fnaive2 = figure;
hold all;
for k = 1:size(blocks,2)
    scatter(matsizes, naive(:,k), 100, 'DisplayName', sprintf('blocksize = %d', blocks(k)),...
    'MarkerEdgeColor', colours(k,:));
end
legend('show', 'Location', 'West');
xlabel('matrix size');
ylabel('seconds');
title('Dependence of runtime (in seconds) on matrix and block size in O(n^3) naive algorithm');
saveas(gcf, '../plots/figs/fnaive2', 'fig');
%% Plot blocked ijk algorithm
fijk = figure;
plot3k(ijk, 'Marker', marker);
ylabel('log_2 of matrix size');
xlabel('log_2 of blocksize');
title('Dependence of runtime (in seconds) on matrix and block size in blocked ijk algorithm');
saveas(gcf, '../plots/figs/fijk', 'fig');

fijk2 = figure;
hold all;
for k = 1:size(blocks,2)
    scatter(matsizes, ijk(:,k), 100, 'DisplayName', sprintf('n = %d', blocks(k)),...
    'MarkerEdgeColor', colours(k,:));
end
legend('show', 'Location', 'West');
xlabel('matrix size');
ylabel('seconds');
title('Dependence of runtime (in seconds) on matrix and block size in blocked ijk algorithm');
saveas(gcf, '../plots/figs/fijk2', 'fig');
%% Plot blocked kij algorithm
fkij = figure;
plot3k(kij, 'Marker', marker, 'ColorRange', [0, 20]);
cbar = findobj(gcf, 'Tag', 'Colorbar');
ytick = get(cbar, 'YTickLabel');
set(cbar, 'YTickLabel', [ytick(1:end-1); {''}]);
ylabel('log_2 of matrix size');
xlabel('log_2 of blocksize');
title('Dependence of runtime (in seconds) on matrix and block size in blocked kij algorithm');
saveas(gcf, '../plots/figs/fkij', 'fig');

fkij2 = figure;
hold all;
for k = 1:size(blocks,2)
    scatter(matsizes, kij(:,k), 100, 'DisplayName', sprintf('n = %d', blocks(k)),...
    'MarkerEdgeColor', colours(k,:));
end
legend('show', 'Location', 'West');
xlabel('matrix size');
ylabel('seconds');
title('Dependence of runtime (in seconds) on matrix and block size in blocked kij algorithm');
saveas(gcf, '../plots/figs/fkij2', 'fig');
%% Plot ATLAS routine
fatlas = figure;
plot3k(atlas, 'Marker', marker, 'ColorRange', [0, 0.11]);
cbar = findobj(gcf, 'Tag', 'Colorbar');
ytick = get(cbar, 'YTickLabel');
set(cbar, 'YTickLabel', [ytick(1:end-1); {''}]);
ylabel('log_2 of matrix size');
xlabel('log_2 of blocksize (no effect)');
title('Dependence of runtime (in seconds) on matrix and block size in ATLAS routine');
saveas(gcf, '../plots/figs/fatlas', 'fig');

fatlas2 = figure;
hold all;
for k = 1:size(blocks,2)
    scatter(matsizes, atlas(:,k), 100, 'DisplayName', sprintf('n = %d', blocks(k)),...
    'MarkerEdgeColor', colours(k,:));
end
legend('show', 'Location', 'West');
xlabel('matrix size');
ylabel('seconds');
title('Dependence of runtime (in seconds) on matrix and block size in ATLAS routine');
saveas(gcf, '../plots/figs/fatlas2', 'fig');
%% Speedup of blocked algorithms
spu1 = figure;
speedup_naive_bijk = naive ./ ijk;
speedup_naive_bijk(speedup_naive_bijk == inf) = nan; % my precision is not always sufficient
plot3k(speedup_naive_bijk, 'Marker', marker);
title('Speedup of blocked ijk over naive ijk algorithm');
xlabel('log_2 of blocksize');
ylabel('log_2 of matrix size');
zlabel('speedup in times');
saveas(gcf, '../plots/figs/spu1', 'fig');

% Compute average speedup
avrg_speedup1 = mean(speedup_naive_bijk(~isnan(speedup_naive_bijk)));

spu2 = figure;
speedup_naive_bkij = naive ./ kij;
plot3k(speedup_naive_bkij, 'Marker', marker);
title('Speedup of blocked kij over naive ijk algorithm');
xlabel('log_2 of blocksize');
ylabel('log_2 of matrix size');
zlabel('speedup in times');
saveas(gcf, '../plots/figs/spu2', 'fig');

avrg_speedup2 = mean(speedup_naive_bkij(~isnan(speedup_naive_bkij)));
%% Speedup of ATLAS routine
spu_atlas = figure;
speedup_kij_atlas = kij ./ atlas;
speedup_kij_atlas(speedup_kij_atlas == inf) = nan; % my precision is not always sufficient
plot3k(speedup_kij_atlas, 'Marker', marker);
title('Speedup of ATLAS over blocked kij algorithm');
xlabel('log_2 of blocksize');
ylabel('log_2 of matrix size');
zlabel('speedup in times');
saveas(gcf, '../plots/figs/spu_atlas', 'fig');

% The larger the blocksize, the smaller the difference gets. ATLAS has a 
% clear edge especially for small blocksizes 